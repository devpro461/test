var Mongoose = require('mongoose'),
    Schema = Mongoose.Schema;

var UserSchema = new Schema({
    email:{ 
        type: String, 
        unique: true
    },
    password:String, 
    role:String, 
    firstName:String, 
    lastName:String, 
    address:String,
    adminId:{ type: Mongoose.Schema.Types.ObjectId, ref: "User" }
})

module.exports = Mongoose.model('User', UserSchema);