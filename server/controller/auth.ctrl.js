const async = require("async");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const nodemailer = require('nodemailer');
const User = require("../model/User");
const config = require("../config/config");
const Utils = require("../Utils/response");

module.exports = {
  /**
   * registeration api
   */
  registration: (req, res) => {
    async.auto(
      {
        encryptPassword: callback => {
          bcrypt.hash(req.body.password, 10, function (err, hashpassword) {
            // encrypt password
            if (err) {
              callback(err);
            } else {
              callback(null, hashpassword);
            }
          });
        },
        insertUser: [
          "encryptPassword",
          (data, callback) => {
            // inser user detail
            req.body.password = data.encryptPassword;
            req.body.role = 'admin'
            User(req.body).save((err, response) => {
              if (err) {
                callback(err);
              } else {
                callback(null, response);
              }
            });
          }
        ]
      },
      (error, result) => {
        if (error) {
          res.status(400).send({
            status: 400,
            error: true,
            message: error,
            success: false
          });
        } else {
          res.status(200).send({
            status: 200,
            error: false,
            success: true,
            message: Utils.ACCOUNT_ADDED,
            data: result.insertUser
          });
        }
      }
    );
  },

  /**
   * Login api
   */
  login: (req, res) => {
    async.auto(
      {
        checkUserEmail: callback => { // check user email exists in db
          User.findOne({ email: req.body.email, role: 'admin' })
            .lean()
            .exec((err, response) => {
              if (err) {
                callback(err);
              } else if (response != null) {
                callback(null, response);
              } else {
                callback(Utils.ACCOUNT_NOT_FOUND);
              }
            });
        },
        checkPasswordAndJWT: [
          "checkUserEmail",
          (data, callback) => {
            bcrypt.compare(
              req.body.password,
              data.checkUserEmail.password,
              function (err, result) { // check password
                if (err) {
                  callback(err);
                } else if (result) {
                  var token = jwt.sign(
                    {
                      email: req.body.email,
                      password: data.checkUserEmail.password,
                      id: data.checkUserEmail._id
                    },
                    config.secret,
                    {
                      expiresIn: 10080
                    }
                  ); // create JWT token

                  callback(null, token);
                } else {
                  callback(Utils.INCORRECT_PASSWORD);
                }
              }
            );
          }
        ]
      },
      (error, result) => {
        if (error) {
          res.status(401).json(Utils.authorisedError(error));
        } else {
          delete result.checkUserEmail.password;
          res
            .status(401)
            .json(
              Utils.successWithToken(
                result.checkUserEmail,
                Utils.LOGIN_SUCCESSFULLY,
                result.checkPasswordAndJWT
              )
            );
        }
      }
    );
  },

  /**
   * Forgot Password
   */

  forgotPassword: (req, res) => {

    async.auto({
      checkUserEmail: (callback) => {
        User.findOne({ email: req.body.email, role: 'admin' }).exec((err, response) => {
          if (err) {
            callback(err)
          } else if (response != null) {
            callback(null, response)
          } else {
            callback(Utils.USER_NOT_FOUND)
          }
        })
      },
      encryptPassword: ['checkUserEmail', (data, callback) => {
        bcrypt.hash(req.body.newpassword, 10, function (err, hashpassword) {
          // encrypt password
          if (err) {
            callback(err);
          } else {
            callback(null, hashpassword);
          }
        });
      }],
      udaptePassword: ['encryptPassword', (data, callback) => {
        var encryptPassword = data.encryptPassword;
        User.findOneAndUpdate(
          { email: req.body.email },
          { $set: { password: encryptPassword } },
          { new: true, upsert: false }
        )
          .lean()
          .exec((err, response) => {
            if (err) {
              callback(err);
            } else {
              callback(null, response);
            }
          });
      }]
    }, (error, result) => {
      if (error) {
        res.status(401).json(Utils.error(error));
      } else {
        res
        .status(401)
        .json(Utils.success(result.udaptePassword, Utils.PASSWORD_UPDATE));
      }
    })
  },
};
