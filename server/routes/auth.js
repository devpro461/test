const { check, validationResult } = require("express-validator");
const authController = require("../controller/auth.ctrl");
const response = require("../Utils/response");
const config = require('../config/config.js')

module.exports = router => {
  function methodNotAllowedHandler(req, res) {
    res.sendStatus(405);
  }
  /**
   * login user
   */
  router
    .route("/register")
    .post(
      [
        check("email")
          .isEmail()
          .withMessage("Enter Valid Email"),
        check("password")
          .isLength({ min: 5 })
          .withMessage(
            "Password must be at least 5 chars long without any spaces"
          )
      ],
      (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(412).json(response.sendError(errors.array()));
        } else {
          authController.registration(req, res);
        }
      }
    )
    .all(methodNotAllowedHandler);

   router
   .route("/login") 
   .post(
    [
      check("email")
        .isEmail()
        .withMessage("Enter Valid Email"),
      check("password")
        .isLength({ min: 5 })
        .withMessage(
          "Password must be at least 5 chars long without any spaces"
        )
    ],
    (req, res) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
      } else {
        authController.login(req, res);
      }
    }
  )
  .all(methodNotAllowedHandler);

  router
   .route("/forgotpassword") 
   .post(
    [
      check("email")
        .isEmail()
        .withMessage("Enter Valid Email"),
        check("newpassword")
        .isLength({ min: 5 })
        .withMessage(
          "Password must be at least 5 chars long without any spaces"
        )
    ],
    (req, res) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
      } else{
        authController.forgotPassword(req, res);
      }
    }
  )
  .all(methodNotAllowedHandler);
};
