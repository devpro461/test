const { check, validationResult } = require("express-validator");
const userController = require("../controller/user.ctrl");
const response = require("../Utils/response");

module.exports = router => {
  function methodNotAllowedHandler(req, res) {
    res.sendStatus(405);
  }
  /**
   * add new user
   */
  router
    .route("/user/add")
    .post(
      [
        check("email")
          .isEmail()
          .withMessage("Enter Valid Email"),
        check("password")
          .isLength({ min: 5 })
          .withMessage(
            "Password must be at least 5 chars long without any spaces"
          ),
        check("firstName")
          .isLength({ min: 2 })
          .trim()
          .matches("^(?!d+$)(?:[a-zA-Z0-9][a-zA-Z0-9 _@&$]*)?$")
          .withMessage("first name must be valid and at least 2 chars long"),
        check("lastName")
          .isLength({ min: 2 })
          .trim()
          .matches("^(?!d+$)(?:[a-zA-Z0-9][a-zA-Z0-9 _@&$]*)?$")
          .withMessage("last name must be valid and at least 2 chars long"),
        check("address")
          .isLength({ min: 2 })
          .trim()
          .matches("^(?!d+$)(?:[a-zA-Z0-9][a-zA-Z0-9 _@&$]*)?$")
          .withMessage("last name must be valid and at least 2 chars long")
      ],
      (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(412).json(response.sendError(errors.array()));
        } else {
          userController.addUser(req, res);
        }
      }
    )
    .all(methodNotAllowedHandler);

  /**
   * udpate user
   */
  router
    .route("/user/update/:id")
    .put(
      [
        check("email")
          .isEmail()
          .withMessage("Enter Valid Email"),
        check("firstName")
          .isLength({ min: 2 })
          .trim()
          .matches("^(?!d+$)(?:[a-zA-Z0-9][a-zA-Z0-9 _@&$]*)?$")
          .withMessage("first name must be valid and at least 2 chars long"),
        check("lastName")
          .isLength({ min: 2 })
          .trim()
          .matches("^(?!d+$)(?:[a-zA-Z0-9][a-zA-Z0-9 _@&$]*)?$")
          .withMessage("last name must be valid and at least 2 chars long"),
        check("address")
          .isLength({ min: 2 })
          .trim()
          .matches("^(?!d+$)(?:[a-zA-Z0-9][a-zA-Z0-9 _@&$]*)?$")
          .withMessage("last name must be valid and at least 2 chars long")
      ],
      (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(412).json(response.sendError(errors.array()));
        } else {
          userController.update(req, res);
        }
      }
    )
    .all(methodNotAllowedHandler);

  /**
   * delete user
   */
  router
    .route("/user/delete/:id")
    .delete(userController.delete)
    .all(methodNotAllowedHandler);
};
