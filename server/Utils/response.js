var message = {
   
    USER_NOT_FOUND: "User not Found",
    ACCOUNT_ADDED:"Registered Successfully",
    ACCOUNT_NOT_FOUND: "Account not found",
    INCORRECT_PASSWORD: "Password not matched",
    LOGIN_SUCCESSFULLY: "login successfully",
    UPDATE_USER: "User udpated successfully",
    DELETE_USER: "Delete user successfully",
    PASSWORD_UPDATE: "Password updated successfully",

    error: (err) => {
        let response = {
            statusCode: 400,
            error: true,
            success: false,
            message: err,
            data: null
        };
        return response;
    },
    authorisedError: (err) => {
        let response = {
            statusCode: 401,
            error: true,
            success: false,
            message: err,
            data: null
        };
        return response;
    },
    sendError: (err) => {
        let response = {
            statusCode: 412,
            message: err,
            error: true,
            errorType: "VALIDATION_ERROR",
            data: null,
            success: false
        };
        return response;
    },
    success: (data, message) => {
        let response = {
            statusCode: 200,
            error: false,
            success: true,
            message: message,
            data: data,
        };
        return response;
    },
    successWithToken: (data, message, token) => {
        let response = {
            statusCode: 200,
            error: false,
            success: true,
            message: message,
            data: data,
            token
        };
        return response;
    },
}

module.exports = message